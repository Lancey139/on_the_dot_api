use serde::{Deserialize, Serialize};
#[derive(Serialize, Deserialize)]
#[derive(Clone)]
pub enum GameState {
    Lobby,
    Running,
    Finished,
    Destroyed
}
