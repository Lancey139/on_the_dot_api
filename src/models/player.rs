use serde::{Deserialize, Serialize};
use std::cmp::Ordering;

#[derive(Clone)]
#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
#[derive(Eq)]
pub struct PlayerModel {
    pub pseudo: String,
    pub is_owner: Option<bool>,
    pub score: Option<u32>
}

impl Ord for PlayerModel {
    fn cmp(&self, other: &Self) -> Ordering {
        self.score.cmp(&other.score)
    }
}

impl PartialOrd for PlayerModel {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for PlayerModel {
    fn eq(&self, other: &Self) -> bool {
        self.score == other.score
    }
}
