use crate::models::game_state::{GameState};
use crate::models::player::PlayerModel;
use crate::on_the_dot_game::game::Game;
use std::collections::{HashMap, BTreeSet};
use actix_web_lab::sse;
use std::io;
use uuid::Uuid;
use log::info;
use std::sync::RwLock;

pub struct GameManager {
    games: RwLock<HashMap<Uuid, Game>>
}

impl GameManager {
    pub fn new() -> Self {
        Self {
            games: RwLock::new(HashMap::new())
        }
    }

    pub fn create_game(&self, new_game_name: &String) -> Result<Uuid, io::Error> {
        let is_game_name_used: bool = self.games.read().unwrap().iter().any( |(_, game)| game.get_game_name() == *new_game_name);

        if is_game_name_used {
            Err(io::Error::new(io::ErrorKind::AlreadyExists, "This game name already exists"))
        } else {
            let new_game = Game::new(new_game_name);
            let new_game_id = new_game.get_game_id().clone();
            info!("New game created {}", new_game_id);
            self.games.write().unwrap().insert(new_game_id, new_game);
            Ok(new_game_id)
        }
    }

    pub fn list_lobby_games(&self) -> Vec<String> {
        let mut result: Vec<String> = Vec::new();
        for (_, game) in self.games.read().unwrap().iter()
        .filter(|&(_, game)| matches!(game.get_game_state(), GameState::Lobby))
        {
            result.push(game.get_game_name());
        }
        result
    }

    pub async fn join_game(&self, game_name: &String, player: PlayerModel) -> Result<Uuid, io::Error> {
        match self.games.read().unwrap().iter().find( |(_, game)| game.get_game_name() == *game_name) {
            Some((_, game_ref)) => {
                game_ref.new_player(player).await
            },
            None => Err(io::Error::new(io::ErrorKind::NotFound, "This game doesn't exist"))
        }
    }

    pub async fn player_left_game(&self, game_id: &Uuid, player_pseudo: String) -> Result<(), io::Error> {
        match self.games.write().unwrap().get(game_id) {
            Some(game_ref) => {
                game_ref.remove_player(player_pseudo).await
            },
            None => Err(io::Error::new(io::ErrorKind::NotFound, "This game doesn't exist"))
        }
    }

    pub fn list_players(&self, game_id: Uuid) -> Result<Vec<PlayerModel>, io::Error> {
        match self.games.read().unwrap().get(&game_id) {
            Some(game_ref) => Ok(game_ref.players.read().unwrap().clone()),
            None => Err(io::Error::new(io::ErrorKind::NotFound, "This game doesn't exist"))
        }
    }

    pub async fn set_game_conf(&self, game_id: Uuid, new_duration_ms: u32) -> Result<(), io::Error> {
        match self.games.read().unwrap().get(&game_id) {
            Some(game_ref) => {
                game_ref.set_game_conf(new_duration_ms).await;
                Ok(())
            },
            None => Err(io::Error::new(io::ErrorKind::NotFound, "This game doesn't exist"))
        }
    }

    async fn set_game_state_internal(&self, game_id: Uuid, game_state: GameState) -> Result<(), io::Error> {
        match self.games.read().unwrap().get(&game_id) {
            Some(game_ref) => {
                game_ref.set_game_state(game_state.clone()).await;
                Ok(())
            },
            None => Err(io::Error::new(io::ErrorKind::NotFound, "This game doesn't exist"))
        }
    }

    pub async fn set_game_state(&self, game_id: Uuid, game_state: GameState) -> Result<(), io::Error> {
        match self.set_game_state_internal(game_id.clone(), game_state.clone()).await {
            Ok(()) => {
                if matches!(game_state, GameState::Destroyed) {
                    info!("Game {} has been deleted", &game_id);
                    self.games.write().unwrap().remove(&game_id);
                    info!("Game count : {}", self.games.read().unwrap().len())
                }
                Ok(())
            }
            Err(error) => Err(error)
        }
    }

    pub async fn set_player_score(&self, game_id: Uuid, player_pseudo : String, player_score : u32) -> Result<(), io::Error> {
        match self.games.read().unwrap().get(&game_id) {
            Some(game_ref) => {
                game_ref.set_player_score(player_pseudo, player_score).await
            },
            None => Err(io::Error::new(io::ErrorKind::NotFound, "This game doesn't exist"))
        }
    }

    pub fn get_score_board(&self, game_id: Uuid) -> Result<BTreeSet<PlayerModel>, io::Error> {
        match self.games.read().unwrap().get(&game_id) {
            Some(game_ref) => {
                game_ref.get_score_board()
            },
            None => Err(io::Error::new(io::ErrorKind::NotFound, "This game doesn't exist"))
        }
    }

    pub async fn subscribe_game(&self, game_id: Uuid) -> Result<sse::Sse<sse::ChannelStream>, io::Error>  {
        match self.games.read().unwrap().get(&game_id) {
            Some(game_ref) => {
                Ok(game_ref.broadcaster.new_client().await)
            },
            None => Err(io::Error::new(io::ErrorKind::NotFound, "This game doesn't exist"))
        }
    }
}