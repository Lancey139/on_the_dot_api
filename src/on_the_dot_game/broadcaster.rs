use actix_web_lab::sse;
use std::sync::{Mutex, Arc};
use tokio::time::{self, Duration};
use futures_util::future;
use log::debug;
use uuid::Uuid;

#[derive(Debug, Clone, Default)]
struct BroadcasterInner {
    clients: Vec<sse::Sender>,
}

pub struct Broadcaster {
    inner: Mutex<BroadcasterInner>,
    stop_ping: Mutex<bool>,
    game_uuid: Mutex<Uuid>
}

impl Broadcaster {
    pub fn create(game_uuid: &Uuid) -> Arc<Self> {
        let this = Arc::new(Broadcaster {
            inner: Mutex::new(BroadcasterInner::default()),
            stop_ping: Mutex::new(false),
            game_uuid: Mutex::new(*game_uuid)
        });
        Broadcaster::spawn_ping(Arc::clone(&this));
        this
    }

    fn spawn_ping(this: Arc<Self>) {
        actix_web::rt::spawn(async move {
            let mut interval = time::interval(Duration::from_secs(1));

            while *(this.stop_ping.lock().unwrap()) == false {
                interval.tick().await;
                this.remove_stale_clients().await;
            }
        });
    }

    async fn remove_stale_clients(&self) {
        let clients = self.inner.lock().unwrap().clients.clone();
        let mut ok_clients = Vec::new();

        for client in clients {
            if client
                .send(sse::Event::Comment("ping".into()))
                .await
                .is_ok()
            {
                ok_clients.push(client.clone());
            }
        }
        debug!("Client count {} on game {}", ok_clients.len(), self.game_uuid.lock().unwrap());
        self.inner.lock().unwrap().clients = ok_clients;
    }

    pub async fn new_client(&self) -> sse::Sse<sse::ChannelStream> {
        debug!("Broadcaster: new client created");
        let (tx, rx) = sse::channel(10);

        tx.send(sse::Data::new("{\"connected\": true}")).await.unwrap();
        self.inner.lock().unwrap().clients.push(tx);
        rx
    }

    /// Broadcasts `msg` to all clients.
    pub async fn broadcast(&self, msg: & str) {
        let clients = self.inner.lock().unwrap().clients.clone();

        let send_futures = clients
            .iter()
            .map(|client| client.send(sse::Data::new(msg)));

        // try to send to all clients, ignoring failures
        // disconnected clients will get swept up by `remove_stale_clients`
        let _ = future::join_all(send_futures).await;
    }

    pub fn stop_spawn_thread(&self) {
        *(self.stop_ping.lock().unwrap()) = true;
    }
}