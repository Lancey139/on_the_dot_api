use std::collections::BTreeSet;
use std::io;
use std::sync::{Arc, RwLockWriteGuard};

use crate::dto::game_events::{PlayerJoinedEvent, GameConfEvent, GameStateEvent, PlayerLeftEvent};
use crate::models::game_state::{GameState};
use crate::models::player::PlayerModel;
use super::broadcaster::Broadcaster;
use uuid::Uuid;
use std::sync::{RwLock};
use log::info;

pub struct Game{
    game_id: Uuid,
    game_name: String,
    game_state: RwLock<GameState>,
    duration_to_reach_ms: RwLock<u32>,
    pub players : RwLock<Vec<PlayerModel>>,
    pub broadcaster: Arc<Broadcaster>
}

impl Game {
    pub fn new(game_name: &String) -> Self {
        let game_id = Uuid::new_v4();
        Self {
            game_id: game_id.clone(),
            game_name: game_name.clone(),
            game_state: RwLock::new(GameState::Lobby),
            duration_to_reach_ms: RwLock::new(0),
            players: RwLock::new(Vec::new()),
            broadcaster: Broadcaster::create(&game_id)
        }
    }

    pub async fn set_game_conf(&self, new_duration: u32) {
        *self.duration_to_reach_ms.write().unwrap() = new_duration;

        // Broadcast the event
        let game_conf_event: GameConfEvent = GameConfEvent {
            game_duration_ms: *self.duration_to_reach_ms.read().unwrap()
        };
        let event_as_string = serde_json::to_string(&game_conf_event).unwrap();
        self.broadcaster.broadcast(event_as_string.as_str()).await;
    }

    pub async fn new_player(&self, new_player: PlayerModel) -> Result<Uuid, io::Error> {
        if !matches!(*self.game_state.read().unwrap(), GameState::Lobby) {
            return Err(io::Error::new(io::ErrorKind::ConnectionRefused, "The game is running or finished"));
        }
        if self.players.read().unwrap().iter().find(|player| player.pseudo == new_player.pseudo.clone()).is_some() {
            return Err(io::Error::new(io::ErrorKind::ConnectionRefused, "A player with the same pseudo already joined"));
        }
        self.players.write().unwrap().push(new_player.clone());

        // Broadcast the event
        let player_event = PlayerJoinedEvent {
            new_player: new_player
        };
        let event_as_string = serde_json::to_string(&player_event).unwrap();
        self.broadcaster.broadcast(event_as_string.as_str()).await;

        Ok(self.game_id.clone())
    }

    pub async fn remove_player(&self, player_pseudo : String) -> Result<(), io::Error> {
        self.players.write().unwrap().retain(|p| p.pseudo != player_pseudo);
        // Broadcast the event
        let player_event = PlayerLeftEvent {
            player_pseudo: player_pseudo
        };
        let event_as_string = serde_json::to_string(&player_event).unwrap();
        self.broadcaster.broadcast(event_as_string.as_str()).await;

        Ok(())
    }

    pub async fn set_player_score(&self, player_pseudo : String, duration_ms : u32) -> Result<(), io::Error> {
        let mut lock_guard = self.players.write().unwrap();
        match lock_guard.iter_mut().find(|player| player.pseudo == player_pseudo.clone()) {
            Some(player) => {
                player.score = Some(self.duration_to_reach_ms.read().unwrap().abs_diff(duration_ms));
                // What happens without the await ?
                self.is_game_finished_handler(player_pseudo, &lock_guard).await;
                Ok(())
            }
            None => Err(io::Error::new(
                io::ErrorKind::InvalidInput,
                 format!("The player {} does not exist in game {}", player_pseudo, self.game_name)
                ))
        }
    }

    async fn is_game_finished_handler<'a>(&self, game_name: String, lock_guard: & RwLockWriteGuard<'a, Vec<PlayerModel>>) {
        if lock_guard.iter().all(|player| player.score.is_some()) {
            info!("Game {} is finished !", game_name);
            self.set_game_state(GameState::Finished).await;
        }
    }

    pub async fn set_game_state(&self, game_state: GameState) {
        *self.game_state.write().unwrap() = game_state.clone();

        if matches!(game_state, GameState::Lobby) {
            self.empty_players_score().await;
        }

        // Broadcast the event
        let game_state_event: GameStateEvent = GameStateEvent {
            game_state: self.game_state.read().unwrap().clone()
        };
        let event_as_string = serde_json::to_string(&game_state_event).unwrap();
        self.broadcaster.broadcast(event_as_string.as_str()).await;
    }

    async fn empty_players_score(&self) {
        self.players.write().unwrap().iter_mut().for_each(|p| {p.score = None;})
    }

    pub fn get_score_board(&self) -> Result<BTreeSet<PlayerModel>, io::Error> {
        let mut ordered_set: BTreeSet<PlayerModel> = BTreeSet::new();
        if !matches!(*self.game_state.read().unwrap(), GameState::Finished) {
            return Err(io::Error::new(io::ErrorKind::InvalidData,
                format!("The game {} is not finished yet ! ", self.game_id)));
        }
        self.players.read().unwrap().iter().for_each(|player| -> () {ordered_set.insert(player.clone());});
        return Ok(ordered_set);
    }

    pub fn get_game_state(&self) -> GameState{
        self.game_state.read().unwrap().clone()
    }

    pub fn get_game_id(&self) -> Uuid{
        self.game_id
    }

    pub fn get_game_name(&self) -> String{
        self.game_name.clone()
    }

}

impl Drop for Game {
    fn drop(&mut self) {
        self.broadcaster.stop_spawn_thread();
    }
}