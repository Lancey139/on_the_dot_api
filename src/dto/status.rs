use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
pub enum State {
    Running,
    Error
}

#[derive(Serialize, Deserialize)]
pub struct Status {
    pub state: State
}