use std::collections::BTreeSet;

use serde::{Deserialize, Serialize};
use uuid::Uuid;
use crate::models::player::PlayerModel;
use crate::models::game_state::GameState;

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CreateGameRequest {
    pub game_name: String,
    pub game_state: GameState,
    pub owner : PlayerModel
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct CreateGameResponse {
    pub game_id: Uuid
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct JoinGameRequest {
    pub game_name: String,
    pub player: PlayerModel
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct LeaveGameRequest {
    pub game_id: Uuid,
    pub player_pseudo: String
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct JoinGameResponse {
    pub game_id: Uuid
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ListPlayersGameResponse {
    pub players: Vec<PlayerModel>
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ListGamesResponse {
    pub games_list: Vec<String>
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SetGameConfRequest {
    pub game_id: Uuid,
    pub game_duration_ms: u32
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SetGameStateRequest {
    pub game_id: Uuid,
    pub game_state: GameState
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct SetPlayerScoreRequest {
    pub game_id: Uuid,
    pub player_pseudo: String,
    pub duration_ms: u32
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ScoreBoardResponse {
    pub game_id: Uuid,
    pub players_ordered: BTreeSet<PlayerModel>
}