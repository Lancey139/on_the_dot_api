use serde::{Deserialize, Serialize};

use crate::models::{player::PlayerModel, game_state::GameState};

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PlayerJoinedEvent  {
    pub new_player: PlayerModel
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PlayerLeftEvent  {
    pub player_pseudo: String
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GameConfEvent {
    pub game_duration_ms: u32
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct GameStateEvent {
    pub game_state: GameState
}
