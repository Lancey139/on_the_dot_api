use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct ErrorMessage {
    pub error_message: String
}

impl ErrorMessage {
    pub fn new(error_message_param: String) -> Self {
        Self {
            error_message: error_message_param
        }
    }
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct InfoMessage {
    pub info_message: String
}

impl InfoMessage {
    pub fn new(info_message_param: String) -> Self {
        Self {
            info_message: info_message_param
        }
    }
}