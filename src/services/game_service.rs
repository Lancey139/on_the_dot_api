use crate::dto::game_request::{JoinGameRequest, JoinGameResponse, ListGamesResponse, CreateGameRequest, CreateGameResponse, ListPlayersGameResponse, SetGameConfRequest, SetGameStateRequest, SetPlayerScoreRequest, ScoreBoardResponse, LeaveGameRequest};
use crate::on_the_dot_game::games_manager::GameManager;
use actix_web_lab::sse;
use std::io;
use uuid::Uuid;

pub struct GameService {
    game_manager: GameManager
}
// Add thread safety here
impl GameService {
    pub fn new() -> Self {
        Self {
            game_manager: GameManager::new()
        }
    }

    pub async fn create_game_service(&self, new_game: CreateGameRequest)-> Result<CreateGameResponse, io::Error> {
        match self.game_manager.create_game(&new_game.game_name) {
            Ok(game_uuid) => {
                if let Err(join_error) = self.game_manager.join_game(&new_game.game_name, new_game.owner).await {
                    return Err(join_error);
                }
                Ok(CreateGameResponse {
                    game_id: game_uuid
                })
            },
            Err(error) => Err(error)
        }
    }

    pub fn list_game_service(&self) -> ListGamesResponse{
        ListGamesResponse {
            games_list: self.game_manager.list_lobby_games()
        }
    }

    pub async fn join_game_service(&self, join_game_request: JoinGameRequest) -> Result<JoinGameResponse, io::Error> {
        match self.game_manager.join_game(&join_game_request.game_name, join_game_request.player).await {
            Ok(game_uuid) => Ok(JoinGameResponse {
                    game_id: game_uuid
                }),
            Err(error) => Err(error)
        }
    }

    pub async fn player_left_game_service(&self, leave_game_request: LeaveGameRequest) -> Result<(), io::Error> {
        self.game_manager.player_left_game(&leave_game_request.game_id, leave_game_request.player_pseudo).await
    }

    pub fn list_players_service(&self, game_id: Uuid) -> Result<ListPlayersGameResponse, io::Error> {

        match self.game_manager.list_players(game_id) {
            Ok(players) => Ok(ListPlayersGameResponse {
                    players: players
                }),
            Err(error) => Err(error)
        }
    }

    pub async fn set_game_conf_service(&self, set_game_conf: SetGameConfRequest) -> Result<(), io::Error> {
        self.game_manager.set_game_conf(set_game_conf.game_id, set_game_conf.game_duration_ms).await
    }

    pub async fn set_game_state_service(&self, set_game_state: SetGameStateRequest) -> Result<(), io::Error> {
        self.game_manager.set_game_state(set_game_state.game_id, set_game_state.game_state).await
    }

    pub async fn set_player_score_service(&self, set_player_score: SetPlayerScoreRequest) -> Result<(), io::Error> {
        self.game_manager.set_player_score(set_player_score.game_id,
             set_player_score.player_pseudo,
              set_player_score.duration_ms).await
    }

    pub fn get_score_board_service(&self, game_id: Uuid) -> Result<ScoreBoardResponse, io::Error> {
        match self.game_manager.get_score_board(game_id) {
            Ok(players_ordered) => Ok(ScoreBoardResponse {
                game_id: game_id,
                players_ordered: players_ordered
            }),
            Err(error) => Err(error)
        }
    }

    pub async fn subscribe_game_service(&self, game_id: Uuid) -> Result<sse::Sse<sse::ChannelStream>, io::Error>  {
        self.game_manager.subscribe_game(game_id).await
    }
}
