use actix_web::{http, web, App, HttpServer, middleware::Logger};
use handlers::game_handlers::{create_game_handler, list_game_handler, join_game_handler, list_players_handler, set_game_conf_handler, set_game_state_handler, set_player_score_handler, get_score_board, player_left_handler};
use handlers::app_state::AppState;
use handlers::sse_handlers::sse_subscribe;
use handlers::status_handlers::status_handler;
use std::sync::{Arc};
use actix_cors::Cors;
use crate::services::game_service::GameService;

pub mod handlers;
pub mod models;
pub mod on_the_dot_game;
pub mod services;
pub mod dto;


#[actix_web::main]
async fn main() -> std::io::Result<()> {
    env_logger::init_from_env(env_logger::Env::new().default_filter_or("debug"));

    let app_data = web::Data::new(
        AppState {
            game_service: Arc::new(GameService::new())
        }
    );

    HttpServer::new(move || {
        let cors = Cors::default()
        .allow_any_origin()
        .allowed_methods(vec!["GET", "POST"])
        .allowed_headers(vec![http::header::AUTHORIZATION, http::header::ACCEPT])
        .allowed_header(http::header::CONTENT_TYPE)
        .max_age(3600);

        App::new()
            .wrap(cors)
            .wrap(Logger::new("%a %{User-Agent}i"))
            .app_data(app_data.clone())
            .service(sse_subscribe)
            .service(
                web::scope("/game")
                .route("/create", web::post().to(create_game_handler))
                .route("/list", web::get().to(list_game_handler))
                .route("/join", web::post().to(join_game_handler))
                .route("/leave", web::post().to(player_left_handler))
                .route("/set_game_conf", web::post().to(set_game_conf_handler))
                .route("/set_game_state", web::post().to(set_game_state_handler))
                .route("/set_player_score", web::post().to(set_player_score_handler))
                .route("/list_players/{game_id}", web::get().to(list_players_handler))
                .route("/score_board/{game_id}", web::get().to(get_score_board))
            )
            .service(status_handler)
    })
    .bind(("127.0.0.1", 8083))?
    .run()
    .await
}