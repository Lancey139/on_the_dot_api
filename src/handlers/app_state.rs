use crate::services::game_service::GameService;
use std::sync::{Arc};

pub struct AppState {
    pub game_service: Arc<GameService>
}
