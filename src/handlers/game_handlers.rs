use actix_web::{HttpResponse, Responder, web};
use crate::handlers::app_state::AppState;
use crate::dto::game_request::{JoinGameRequest, SetGameConfRequest, SetGameStateRequest, SetPlayerScoreRequest, LeaveGameRequest};
use uuid::Uuid;

pub async fn create_game_handler(data: web::Data<AppState>, req_body: String) -> impl Responder {
    let new_game = serde_json::from_str(&req_body);
    match new_game {
        Ok(game) => {
            match data.game_service.create_game_service(game).await {
                Ok(response) => HttpResponse::Ok().json(response),
                Err(error) => HttpResponse::Forbidden().body(error.to_string())
            }
        },
        Err(error) =>
        HttpResponse::BadRequest().body(format!("Malformated data: {}", error.to_string())),
    }
}

pub async fn list_game_handler(data: web::Data<AppState>) -> impl Responder {
    let game_list =  data.game_service.list_game_service();
    HttpResponse::Ok().json(game_list)
}

pub async fn join_game_handler(data: web::Data<AppState>, req_body: String) -> impl Responder {
    let join_game: Result<JoinGameRequest, serde_json::Error> = serde_json::from_str(&req_body);
    if join_game.is_err() {
        return HttpResponse::BadRequest().body(format!("Malformated data: {}", join_game.err().unwrap()))
    }
    match data.game_service.join_game_service(join_game.unwrap()).await {
        Ok(response) => HttpResponse::Ok().json(response),
        Err(error) => HttpResponse::Forbidden().body(error.to_string())
    }
}

pub async fn player_left_handler(data: web::Data<AppState>, req_body: String) -> impl Responder {
    let leave_game: Result<LeaveGameRequest, serde_json::Error> = serde_json::from_str(&req_body);
    if leave_game.is_err() {
        return HttpResponse::BadRequest().body(format!("Malformated data: {}", leave_game.err().unwrap()))
    }
    match data.game_service.player_left_game_service(leave_game.unwrap()).await {
        Ok(response) => HttpResponse::Ok().json(response),
        Err(error) => HttpResponse::Forbidden().body(error.to_string())
    }
}

pub async fn set_game_conf_handler(data: web::Data<AppState>, req_body: String) -> impl Responder {
    let game_conf: Result<SetGameConfRequest, serde_json::Error> = serde_json::from_str(&req_body);
    if game_conf.is_err() {
        return HttpResponse::BadRequest().body(format!("Malformated data: {}", game_conf.err().unwrap()))
    }
    match data.game_service.set_game_conf_service(game_conf.unwrap()).await {
        Ok(response) => HttpResponse::Ok().json(response),
        Err(error) => HttpResponse::Forbidden().body(error.to_string())
    }
}

pub async fn set_game_state_handler(data: web::Data<AppState>, req_body: String) -> impl Responder {
    let game_state: Result<SetGameStateRequest, serde_json::Error> = serde_json::from_str(&req_body);
    if game_state.is_err() {
        return HttpResponse::BadRequest().body(format!("Malformated data: {}", game_state.err().unwrap()))
    }
    match data.game_service.set_game_state_service(game_state.unwrap()).await {
        Ok(response) => HttpResponse::Ok().json(response),
        Err(error) => HttpResponse::Forbidden().body(error.to_string())
    }
}
pub async fn set_player_score_handler(data: web::Data<AppState>, req_body: String) -> impl Responder {
    let player_score_request: Result<SetPlayerScoreRequest, serde_json::Error> = serde_json::from_str(&req_body);
    if player_score_request.is_err() {
        return HttpResponse::BadRequest().body(format!("Malformated data: {}", player_score_request.err().unwrap()))
    }
    match data.game_service.set_player_score_service(player_score_request.unwrap()).await {
        Ok(response) => HttpResponse::Ok().json(response),
        Err(error) => HttpResponse::Forbidden().body(error.to_string())
    }
}

pub async fn list_players_handler(data: web::Data<AppState>, path: web::Path<Uuid>) -> impl Responder {
    let game_id: Uuid = path.into_inner();

    match data.game_service.list_players_service(game_id) {
        Ok(response) => HttpResponse::Ok().json(response),
        Err(error) => HttpResponse::Forbidden().body(error.to_string())
    }
}

pub async fn get_score_board(data: web::Data<AppState>, path: web::Path<Uuid>) -> impl Responder {
    let game_id: Uuid = path.into_inner();
    match data.game_service.get_score_board_service(game_id) {
        Ok(score_board) => HttpResponse::Ok().json(score_board),
        Err(error) => HttpResponse::BadRequest().body(format!("Malformated data: {}", error))
    }
}