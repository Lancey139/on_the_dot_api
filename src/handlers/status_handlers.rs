use actix_web::{get, HttpResponse, Responder};
use crate::dto::status::{State, Status};

#[get("/")]
pub async fn status_handler() -> impl Responder {
    let status = Status {
        state: State::Running
    };
    HttpResponse::Ok().json(status)
}