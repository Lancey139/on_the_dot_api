use crate::handlers::app_state::AppState;
use actix_web::{web, Responder};
use actix_web::get;
use uuid::Uuid;
use actix_web_lab::sse;


#[get("/subscribe/{game_id}")]
pub async fn sse_subscribe(data: web::Data<AppState>, path: web::Path<Uuid>) -> impl Responder {
    let game_id: Uuid = path.into_inner();
    let result = data.game_service.subscribe_game_service(game_id).await;
    if result.is_err() {
        // Send an empty channel in case of error
        return sse::channel(10).1;
    }
    result.unwrap()
}

